using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Generator
{
    protected int seed;
    protected int width;
    protected int height;

    public Generator(int seed, int width, int height)
    {
        this.seed = seed;
        this.width = width;
        this.height = height;
        if(seed != 0)
            Random.InitState(seed);
    }

    public abstract TileDescription[,] Generate();
}
