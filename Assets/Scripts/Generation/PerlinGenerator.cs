using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinGenerator : Generator
{
    public enum PerlinGenerationMode
    {
        ALL_WALL,
        ALL_HOLE,
        MIXED
    }

    protected float scale;
    protected float lowTresh;
    protected float highTresh;
    protected float noiseXOffset;
    protected float noiseYOffset;
    protected PerlinGenerationMode mode;
    protected OpenSimplexNoise simplexNoise;
    public float[,] valuesMatrix;

    public PerlinGenerator(int seed, int width, int height, float scale,
        float lowTresh = 0.3f, float highTresh = 0.7f, PerlinGenerationMode mode = PerlinGenerationMode.MIXED)
        : base(seed, width, height)
    {
        this.scale = scale;
        this.lowTresh = lowTresh;
        this.highTresh = highTresh;
        this.mode = mode;
        simplexNoise = new OpenSimplexNoise();
        noiseXOffset = Random.value * 20000 - 10000;
        noiseYOffset = Random.value * 20000 - 10000;
        Debug.Log(noiseXOffset + " " + noiseYOffset + " " + width + " " + height + " " + scale);
    }

    public override TileDescription[,] Generate()
    {
        Vector2Int mSize = new Vector2Int(width, height);
        float[,] generatedValues = new float[mSize.x, mSize.y];

        for (int i = 0; i < mSize.x; i++)
        {
            for (int j = 0; j < mSize.y; j++)
            {
                generatedValues[i, j] = getNoiseValue(i, j, mSize, scale);
            }
        }
        this.valuesMatrix = generatedValues;
        return interpreteValues(generatedValues, mSize);
    }

    private TileDescription[,] interpreteValues(float[,] generatedValues, Vector2Int mSize)
    {
        TileDescription[,] res = new TileDescription[mSize.x, mSize.y];
        for (int i = 0; i < mSize.x; i++)
        {
            for (int j = 0; j < mSize.y; j++)
            {
                float value = generatedValues[i, j];
                if (value > highTresh)
                {
                    if (mode == PerlinGenerationMode.ALL_HOLE)
                    {
                        res[i, j] = new TileDescription(TileType.EMPTY);
                    }
                    else
                    {
                        res[i, j] = new TileDescription(TileType.DEFAULT_WALL);
                    }
                }
                else if (value > lowTresh)
                {
                    res[i, j] = new TileDescription(TileType.DEFAULT);
                }
                else
                {
                    if (mode == PerlinGenerationMode.ALL_WALL)
                    {
                        res[i, j] = new TileDescription(TileType.DEFAULT_WALL);
                    }
                    else
                    {
                        res[i, j] = new TileDescription(TileType.EMPTY);
                    }
                }
            }
        }
        return res;
    }

    private float[,] smoothMatrix(float[,] m, Vector2Int mSize, float[,] filter, Vector2Int fSize)
    {
        float[,] tmp = Utilities.convolution(m, mSize, filter, fSize);
        Vector2Int newSize = new Vector2Int(mSize.x - fSize.x + 1, mSize.y - fSize.y + 1);
        return Utilities.crop(tmp, newSize);
    }

    private float getNoiseValue(float x, float y, Vector2Int size, float scale)
    {

        Vector2 tpos = new Vector2(x, y);
        float angle = 30;
        //float angle = (Mathf.PI * (Mathf.PerlinNoise((x + 1000) * 5.11f, (y + 1000) * 5.11f) - 0.5f)) * 2;
        tpos = rotate(x, y , angle);

        float xCoord = noiseXOffset + tpos.x/size.x * scale; //the constant value is here because
        float yCoord = noiseYOffset + tpos.y/size.y * scale;  //perlin noise always provide same value on integer coords


        float value = Mathf.PerlinNoise(xCoord, yCoord);
        return value;
    }

    private Vector2 rotate(float x, float y, float angle)
    {
        float sin = Mathf.Sin(angle * Mathf.Deg2Rad);
        float cos = Mathf.Cos(angle * Mathf.Deg2Rad);

        float tx = (cos * x) - (sin * y);
        float ty = (sin * x) + (cos * y);
        return new Vector2(tx, ty);
    }
}
