using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugGenerator : Generator
{
    public DebugGenerator(ref int w, ref int h) : base(10,10,10)
    {
        w = 10;
        h = 10;
    }
    public override TileDescription[,] Generate()
    {
        int[,] generation = {
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},// j ->
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},// i
            { 1, 1, 1, 1, 1, 1, 2, 2, 2, 1},// |
            { 1, 1, 1, 1, 1, 1, 1, 2, 1, 1},// v
            { 1, 1, 1, 1, 1, 1, 1, 3, 1, 1},
            { 1, 1, 1, 1, 1, 3, 3, 3, 1, 1},
            { 1, 1, 3, 3, 3, 3, 3, 1, 1, 1},
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        };
        return interprete(generation, 10, 10);
    }

    TileDescription[,] interprete(int[,] gen, int w, int h)
    {
        TileDescription[,] res = new TileDescription[w, h];
        for (int i = 0; i < w; i++)
        {
            for (int j = 0; j < h; j++)
            {
                switch(gen[i,j])
                {
                    case 1:
                        res[i, j] = new TileDescription(TileType.DEFAULT);
                        break;
                    case 2:
                        res[i, j] = new TileDescription(TileType.DEFAULT_WALL);
                        break;
                    case 3:
                        res[i, j] = new TileDescription(TileType.WATER);
                        break;
                    default: //empty
                        res[i, j] = new TileDescription(TileType.EMPTY);
                        break;
                }
            }
        }
        return res;
    }
}
