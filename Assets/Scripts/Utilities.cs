using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    #region values

    public static float yTileOffset = Mathf.Sqrt(0.75f);

    public static float[,] identity = {
            { 0, 0, 0 },
            { 0, 1, 0 },
            { 0, 0, 0 }
        };
    public static Vector2Int identitySize = new Vector2Int(3, 3);
    public static float[,] gaussBlur3x3 = {
            { 1, 2, 1 },
            { 2, 4, 2 },
            { 1, 2, 1 }
        };
    public static Vector2Int gaussBlur3x3Size = new Vector2Int(3, 3);
    public static float[,] gaussBlur5x5 = {
            { 1, 4, 6, 4, 1 },
            { 4, 16, 48, 16, 4 },
            { 6, 24, 36, 24, 6 },
            { 4, 16, 48, 16, 4 },
            { 1, 4, 6, 4, 1 }
        };
    public static Vector2Int gaussBlur5x5Size = new Vector2Int(5, 5);
    public static float[,] sharpnessIncreaser = {
            { 0, -1, 0 },
            {-1, 5, -1 },
            { 0,-1, 0 }
        };
    public static Vector2Int sharpnessIncreaserSize = new Vector2Int(3, 3);
    public static float[,] edgeDetection = {
            { 1, -1, -1 },
            {-1, 8, -1 },
            { -1,-1, -1 }
        };
    public static Vector2Int edgeDetectionSize = new Vector2Int(3, 3);
    #endregion

    #region functions
    public static float[,] convolution(float[,] matrix, Vector2Int mSise, float[,] filter, Vector2Int fSize)
    {
        //error managment
        if (mSise.x == 0 || mSise.y == 0)
        {
            Debug.LogError("matrice size should be greater than 0");
        } else if (fSize.x == 0 || fSize.y == 0 || fSize.x % 2 == 0 || fSize.y % 2 == 0)
        {
            Debug.LogError("filter size should be greater than 0 and odd");
        }

        Vector2Int resSize = new Vector2Int(mSise.x - fSize.x + 1, mSise.y - fSize.y + 1);
        Vector2Int centerShift = new Vector2Int(fSize.x / 2, fSize.y / 2);
        float[,] res = new float[resSize.x, resSize.y];

        for(int i = 0; i < resSize.x; i++)
        {
            for (int j = 0; j < resSize.y; j++)
            {
                res[i, j] = 0;
                float coef = 0;
                //apply filter
                for (int a = -centerShift.x; a < centerShift.x; a++)
                {
                    for (int b = -centerShift.y; b < centerShift.y; b++)
                    {
                        float fValue = filter[a + centerShift.x, b + centerShift.y];
                        coef += fValue;
                        res[i, j] = matrix[i + a + centerShift.x, j + b + centerShift.y] * fValue;
                    }
                }
                if(coef != 0)
                    res[i, j] *= 1 / coef;
            }
        }
        return res;
    }

    public static float[,] crop(float[,] matrix, Vector2Int mSise)
    {
        float min = float.MaxValue;
        float max = float.MinValue;
        for (int i = 0; i < mSise.x; i++)
        {
            for (int j = 0; j < mSise.y; j++)
            {
                min = Mathf.Min(matrix[i, j], min);
                max = Mathf.Max(matrix[i, j], max);
            }
        }

        float coef = 1 / max;
        float[,] res = new float[mSise.x, mSise.y];

        for (int i = 0; i < mSise.x; i++)
        {
            for (int j = 0; j < mSise.y; j++)
            {
                res[i, j] = (matrix[i, j] - min) * coef;
            }
        }
        return res;
    }

    public static Vector2 tileCoordsToWorldPosition(Vector2Int coords)
    {
        float xOffset = (coords.y % 2 == 0) ? 0 : 0.5f;
        return new Vector2(coords.x + xOffset, coords.y * yTileOffset);
    }
    #endregion
}
