using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder
{
    private class Vector2IntNode : FastPriorityQueueNode
    {
        public Vector2Int Value;
        public Vector2IntNode(Vector2Int value)
        {
            this.Value = value;
        }
        public override bool Equals(object obj)
        {
            // If the passed object is null
            if (obj == null)
            {
                return false;
            }
            if (!(obj is Vector2IntNode))
            {
                return false;
            }
            return (this.Value == ((Vector2IntNode)obj).Value);
        }
        public override int GetHashCode()
        {
            return this.Priority.GetHashCode() ^ this.Value.GetHashCode();
        }
    }
    public static List<Vector2Int> AStar(Dictionary<Vector2Int, GameObject> board, Vector2Int startPos, Vector2Int endPos)
    {
        // The set of discovered nodes that may need to be (re-)expanded.
        // Initially, only the start node is known.
        // This is usually implemented as a min-heap or priority queue rather than a hash-set.
        FastPriorityQueue<Vector2IntNode> openSet = new FastPriorityQueue<Vector2IntNode>(board.Count);
        openSet.Enqueue(new Vector2IntNode(startPos),0);

        // For node n, cameFrom[n] is the node immediately preceding it on the cheapest path from start
        // to n currently known.
        Dictionary<Vector2Int, Vector2Int> cameFrom = new Dictionary<Vector2Int, Vector2Int>();

        // For node n, gScore[n] is the cost of the cheapest path from start to n currently known.
        Dictionary<Vector2Int, float> gScore = new Dictionary<Vector2Int, float>();
        gScore[startPos] = 0;

        // For node n, fScore[n] := gScore[n] + h(n). fScore[n] represents our current best guess as to
        // how short a path from start to finish can be if it goes through n.
        Dictionary<Vector2Int, float> fScore = new Dictionary<Vector2Int, float>();
        fScore[startPos] = heuristic(startPos, endPos);

        while(openSet.Count > 0)
        {
            Debug.Log("a");
            // This operation can occur in O(1) time if openSet is a min-heap or a priority queue
            Vector2Int current = openSet.Dequeue().Value;
            if(current == endPos)
            {
                return reconstruct_path(cameFrom, current);
            }

            List<Vector2Int> neighbors = getNeighbor(board, current);
            foreach (Vector2Int n in neighbors)
            {
                // d(current,neighbor) is the weight of the edge from current to neighbor
                // tentative_gScore is the distance from start to the neighbor through current
                float tentative_gScore = gScore[current] + transitionCost(board, current, n);
                if(tentative_gScore < (gScore.ContainsKey(n)?gScore[n] : float.MaxValue))
                {
                    // This path to neighbor is better than any previous one. Record it!
                    cameFrom[n] = current;
                    gScore[n] = tentative_gScore;
                    fScore[n] = gScore[n] + heuristic(current, n);
                    Vector2IntNode newNode = new Vector2IntNode(n);
                    if (!openSet.Contains(newNode))
                        openSet.Enqueue(newNode, fScore[n]);
                }
            }
        }

        return null;
    }

    private static float heuristic(Vector2Int a, Vector2Int b)
    {
        return 0;
    }

    public static List<Vector2Int> getNeighbor(Dictionary<Vector2Int, GameObject> board, Vector2Int tile)
    {
        List<Vector2Int> res = new List<Vector2Int>();
        List<Vector2Int> neighbors = new List<Vector2Int>();

        if (tile.y % 2 == 1)
        {
            neighbors.Add(new Vector2Int(-1, 0));
            neighbors.Add(new Vector2Int(0, -1));
            neighbors.Add(new Vector2Int(0, 1));
            neighbors.Add(new Vector2Int(1, -1));
            neighbors.Add(new Vector2Int(1, 0));
            neighbors.Add(new Vector2Int(1, 1));
        } else
        {
            neighbors.Add(new Vector2Int(-1, -1));
            neighbors.Add(new Vector2Int(-1, 0));
            neighbors.Add(new Vector2Int(-1, 1));
            neighbors.Add(new Vector2Int(0, -1));
            neighbors.Add(new Vector2Int(0, 1));
            neighbors.Add(new Vector2Int(1, 0));

        }

        foreach( Vector2Int n in neighbors)
        {
            Vector2Int target = tile + n;
            if (board.ContainsKey(target) && board[target].GetComponent<Tile>().description.isAccessible())
                res.Add(target);
        }
        return res;
    }

    private static List<Vector2Int> reconstruct_path(Dictionary<Vector2Int, Vector2Int> cameFrom, Vector2Int endTile)
    {
        List<Vector2Int> resPath = new List<Vector2Int>();
        Vector2Int current = endTile;

        resPath.Add(current);
        while (cameFrom.ContainsKey(current))
        {
            current = cameFrom[current];
            resPath.Add(current);
        }
        resPath.Reverse();
        return resPath;
    }

    public static int transitionCost(Dictionary<Vector2Int, GameObject> board, Vector2Int p1, Vector2Int p2)
    {
        TileDescription t1 = board[p1].GetComponent<Tile>().description;
        TileDescription t2 = board[p2].GetComponent<Tile>().description;

        if (t1.type == TileType.DEFAULT && t2.type == TileType.WATER) //default to water
        {
            return 2;
        } else if (t1.type == TileType.WATER && t2.type == TileType.WATER) //water to water
        {
            return 2;
        } else {
            return 1;
        }
    }
}
