using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float hTranslate = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float vTranslate = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        float zoom = Input.GetAxis("Scroll") * -1;

        transform.Translate(Quaternion.Euler(-transform.localRotation.eulerAngles.x, 0, 0) * new Vector3(hTranslate, 0, vTranslate));
        GetComponent<Camera>().orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize + zoom,0.2f,10);
    }
}
