using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    DEFAULT,
    DEFAULT_WALL,
    EMPTY,
    WATER
}
