using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexagonTile : MonoBehaviour
{
    [SerializeField]
    private Tile tile;

    void OnMouseEnter()
    {
        tile.highLight();
    }
    void OnMouseExit()
    {
        tile.unHighLight();
    }

    private void OnMouseDown()
    {
        tile.cliced();
    }
}
