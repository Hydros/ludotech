using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileDescription 
{
    public TileType type = TileType.DEFAULT;

    public TileDescription(TileType type = TileType.DEFAULT)
    {
        this.type = type;
    }

    public bool isAccessible()
    {
        if (type == TileType.EMPTY || type == TileType.DEFAULT_WALL)
            return false;
        else
            return true;
    }
}
