using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject tilePrefab;
    public int seed = 0;
    public int width = 10;
    public int height = 10;

    private Texture2D t;
    public Renderer r;

    Dictionary<Vector2Int, GameObject> board;

    //test
    Vector2Int startPos = new Vector2Int(-1000, 1000);
    Vector2Int endPos = new Vector2Int(-1000, 1000);

    //end test

    void Start()
    {
        board = new Dictionary<Vector2Int, GameObject>();
        UpdateGeneration();
    }

    public void UpdateGeneration()
    {
        if(board.Count >= 0)
        {
            foreach(KeyValuePair<Vector2Int, GameObject> k in board)
            {
                Destroy(k.Value);
            }
            board.Clear();
        }

        Generator generator = new DebugGenerator(ref width, ref height);//new PerlinGenerator(seed, width, height, 10, 0.4f, 0.8f, PerlinGenerator.PerlinGenerationMode.MIXED);
        TileDescription[,] generationMatrix = generator.Generate();

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                addTile(new Vector2Int(i, j), generationMatrix[i, j]);
            }
        }
    }

    public void addTile(Vector2Int pos, TileDescription tileDescr)
    {
        if (!board.ContainsKey(pos))
        {
            Vector2 worldPos2D = Utilities.tileCoordsToWorldPosition(pos);
            Vector3 worldpos = new Vector3(worldPos2D.x, 0, worldPos2D.y);
            GameObject tileObject = Object.Instantiate(tilePrefab, worldpos, Quaternion.identity);
            Tile tile = tileObject.GetComponent<Tile>();
            tile.description = tileDescr;
            tile.position = pos;
            tile.board = this;

            tileObject.transform.parent = gameObject.transform;
            board.Add(pos, tileObject);
        } else
        {
            Debug.LogWarning("key " + pos + " already exist in board");
        }
    }

    public void pathFindingTest(Vector2Int position)
    {
        if(endPos != new Vector2Int(-1000, 1000))
        {
            Debug.Log("newStart");
            endPos = new Vector2Int(-1000,1000);
            startPos = position;
            foreach(KeyValuePair<Vector2Int, GameObject> t in board)
            {
                t.Value.GetComponent<Tile>().unHighLight();
            }
        }
        else if(startPos == new Vector2Int(-1000, 1000))
        {
            Debug.Log("Start");
            startPos = position;
        }
        else if(endPos == new Vector2Int(-1000, 1000))
        {
            Debug.Log("end, process pathfinding");
            endPos = position;
            List<Vector2Int> path = PathFinder.AStar(board, startPos, endPos);
            if(path != null)
                foreach(Vector2Int p in path)
                {
                    board[p].GetComponent<Tile>().highLight();
                }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(Vector3.zero, 0.5f);
    }
}
