using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private TileDescription _description;
    private Board _board;
    public Vector2Int position;
    
    [SerializeField]
    private GameObject tileObject;
    [SerializeField]
    private GameObject tileHighlighter;

    public TileDescription description
    {
        get { return _description; }
        set { this._description = value; updateTile(); }
    }
    public Board board
    {
        get { return _board; }
        set {
            if(this._board == null)
                this._board = value;
        }
    }

    public void updateTile()
    {
        tileObject.SetActive(true);
        tileObject.GetComponent<MeshCollider>().enabled = true;
        tileObject.GetComponent<Renderer>().material.SetColor("_MainColor", new Color(.79f, .79f, .79f, 0));

        switch (_description.type)
        {
            case TileType.DEFAULT:
                tileObject.transform.localScale = new Vector3(0.98f, 13, 0.98f);
                break;
            case TileType.DEFAULT_WALL:
                tileObject.transform.localScale = new Vector3(1, 25, 1);
                tileObject.GetComponent<MeshCollider>().enabled = false;
                tileObject.GetComponent<Renderer>().material.SetColor("_MainColor", new Color(.55f, .55f, .55f, 0));
                break;
            case TileType.WATER:
                tileObject.transform.localScale = new Vector3(1, 12.5f, 1);
                tileObject.GetComponent<Renderer>().material.SetColor("_MainColor", new Color(.2f, .82f, .83f, 0));
                break;
            default: //EMPTY tile
                tileObject.SetActive(false);
                break;
        }
    }

    public void highLight()
    {
        tileHighlighter.SetActive(true);
    }

    public void unHighLight()
    {
        tileHighlighter.SetActive(false);
    }

    public void cliced()
    {
        board.pathFindingTest(position);
    }
    
}
